jQuery(function(){
    jQuery('.starsfield').each(function(){
        var rate = jQuery(this).find('.hidden').val();
        jQuery(this).find('.star').each(function(){
            var current = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
            if(current <= rate) {
                jQuery(this).addClass('selected');
                jQuery(this).removeClass('unselected');
            } else {
                jQuery(this).addClass('unselected');
                jQuery(this).removeClass('selected');
            }
        });
    });
    jQuery('.starsfield .star').click(function(){
      var rate = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
      jQuery(this).closest('.starsfield').find('.star').each(function(){
          var current = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
          if(current <= rate) {
              jQuery(this).addClass('selected');
              jQuery(this).removeClass('unselected');
          } else {
              jQuery(this).addClass('unselected');
              jQuery(this).removeClass('selected');
          }
      });
      jQuery(this).closest('.starsfield').find('.hidden').val(rate);
    });
        jQuery('.starsfield .star').hover(function(){
            var rate = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
            jQuery(this).closest('.starsfield').find('.star').each(function(){
                var current = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
                if(current <= rate) {
                    jQuery(this).addClass('selected');
                    jQuery(this).removeClass('unselected');
                } else {
                    jQuery(this).addClass('unselected');
                    jQuery(this).removeClass('selected');
                }
            });
        },
        function(){
            var rate = jQuery(this).closest('.starsfield').find('.hidden').val();
            jQuery(this).closest('.starsfield').find('.star').each(function(){
                var current = jQuery(this).closest('.starsfield').find('.star').index(jQuery(this)) + 1;
                if(current <= rate) {
                    jQuery(this).addClass('selected');
                    jQuery(this).removeClass('unselected');
                } else {
                    jQuery(this).addClass('unselected');
                    jQuery(this).removeClass('selected');
                }
            });
        }
    );
});