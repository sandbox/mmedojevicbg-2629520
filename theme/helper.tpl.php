<div class="starsfield-render">
    <?php
    for($i = 1; $i <= 5; $i++) {
        if($i <= $rating) {
            ?>
                <div class="star selected"></div>
            <?php
        } else {
            ?>
                <div class="star unselected"></div>
            <?php
        }
    }
    ?>
</div>